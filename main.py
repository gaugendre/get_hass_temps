import os
import sys

from dotenv import load_dotenv
import pendulum
import requests

load_dotenv()

BASE_URL = os.getenv("HASS_BASE_URL")
TOKEN = os.getenv("HASS_TOKEN")

def main():
    assert BASE_URL, "Must supply HASS_BASE_URL env variable"
    assert TOKEN, "Must supply HASS_TOKEN env variable"

    try:
        client = requests.Session()
        client.headers.update({
            "Authorization": f"Bearer {TOKEN}",
            "Content-Type": "application/json",
        })

        url = f"{BASE_URL}/states/sensor.chambre_temperature"
        bedroom = client.get(url, timeout=5)
        bedroom.raise_for_status()
        bedroom = bedroom.json()

        url = f"{BASE_URL}/states/sensor.exterieur_temperature"
        outside = client.get(url, timeout=5)
        outside.raise_for_status()
        outside = outside.json()
    except requests.exceptions.ConnectionError:
        client.close()
        print("Erreur lors de la connexion au serveur.")
        sys.exit(1)
    except Exception as e:
        client.close()
        print(f"Erreur inconnue: {e}")
        sys.exit(1)

    print(format_temp(bedroom))
    print(format_temp(outside))

    temp_bedroom = float(bedroom["state"])
    temp_outside = float(outside["state"])
    print()
    if temp_outside < temp_bedroom:
        print("Il fait plus FRAIS dehors, tu peux ouvrir !")
    else:
        print("FERME TOUT !")


def format_temp(data: dict) -> str:
    name = data["attributes"]["friendly_name"].lower().replace("temperature", "").strip()
    diff = pendulum.parse(data["attributes"]["last_successful_update"]).diff_for_humans(locale="fr")
    return f"{name} : {data['state']}{data['attributes']['unit_of_measurement']} ({diff})"



if __name__ == "__main__":
    main()

